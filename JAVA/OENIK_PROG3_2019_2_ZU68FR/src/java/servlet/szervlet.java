/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tomi
 */
@WebServlet(name = "szervlet", urlPatterns = {"/szervlet"})
public class szervlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Random rand = new Random();
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String mogyi=request.getParameter("mogyi");
            if (mogyi == ""){
                mogyi="Legközelebb válassz mogyit >:(";
                out.println("<zoo>");
                out.println("<uzenet>"+mogyi+"</uzenet>");
                out.println("<eredmeny>");
                out.println("...");
                out.println("</eredmeny>");
                out.println("</zoo>");
            }
            else
            {
                if (rand.nextInt(10)+1 == 5) {
                    mogyi="Sajnáljuk de a kért mogyi elfogyott";
                    out.println("<zoo>");
                    out.println("<uzenet>"+mogyi+"</uzenet>");
                    out.println("<eredmeny>");
                    out.println("0");
                    out.println("</eredmeny>");
                    out.println("</zoo>");
                }
                else
                {
                    mogyi="Az ön általá kért mogyi: "+mogyi;
                    out.println("<zoo>");
                    out.println("<uzenet>"+mogyi+"</uzenet>");
                    out.println("<eredmeny>");
                    out.println(rand.nextInt(20)+1);
                    out.println("</eredmeny>");
                    out.println("</zoo>");
                }
            }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
