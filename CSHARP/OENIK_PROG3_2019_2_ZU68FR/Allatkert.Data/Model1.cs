// <copyright file="Model1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System.Data.Entity;

    /// <summary>
    /// VS generated class.
    /// </summary>
    public partial class Model1 : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Model1"/> class.
        /// </summary>
        public Model1()
            : base("name=Model1")
        {
        }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual DbSet<Allatfajok> Allatfajok { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual DbSet<Allatok> Allatok { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual DbSet<Gondozok> Gondozok { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual DbSet<Kifutok> Kifutok { get; set; }

        /// <summary>
        /// does good stuff.
        /// </summary>
        /// <param name="modelBuilder">i don't know sorry.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Allatfajok>()
                .Property(e => e.FajID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Allatfajok>()
                .Property(e => e.FajNev)
                .IsUnicode(false);

            modelBuilder.Entity<Allatfajok>()
                .Property(e => e.Taplalkozas)
                .IsUnicode(false);

            modelBuilder.Entity<Allatfajok>()
                .HasMany(e => e.Allatok)
                .WithRequired(e => e.Allatfajok)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Allatok>()
                .Property(e => e.AllatID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Allatok>()
                .Property(e => e.AllatNev)
                .IsUnicode(false);

            modelBuilder.Entity<Allatok>()
                .Property(e => e.FajID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Allatok>()
                .Property(e => e.KifutoID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Allatok>()
                .Property(e => e.Nem)
                .IsUnicode(false);

            modelBuilder.Entity<Gondozok>()
                .Property(e => e.GondozoID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Gondozok>()
                .Property(e => e.GondozoNev)
                .IsUnicode(false);

            modelBuilder.Entity<Gondozok>()
                .Property(e => e.Telefonszam)
                .IsUnicode(false);

            modelBuilder.Entity<Gondozok>()
                .HasMany(e => e.Kifutok)
                .WithRequired(e => e.Gondozok)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kifutok>()
                .Property(e => e.KifutoID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Kifutok>()
                .Property(e => e.KifutoNev)
                .IsUnicode(false);

            modelBuilder.Entity<Kifutok>()
                .Property(e => e.KifutoMeret)
                .IsUnicode(false);

            modelBuilder.Entity<Kifutok>()
                .Property(e => e.GondozoID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Kifutok>()
                .HasMany(e => e.Allatok)
                .WithRequired(e => e.Kifutok)
                .WillCascadeOnDelete(false);
        }
    }
}
