// <copyright file="Kifutok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// VS generated stuff.
    /// </summary>
    [Table("Kifutok")]
    public partial class Kifutok
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Kifutok"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kifutok()
        {
            this.Allatok = new HashSet<Allatok>();
        }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        [Key]
        [Column(TypeName = "numeric")]
        public decimal KifutoID { get; set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        [Required]
        [StringLength(30)]
        public string KifutoNev { get; set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        [Required]
        [StringLength(30)]
        public string KifutoMeret { get; set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal GondozoID { get; set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Allatok> Allatok { get; set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public virtual Gondozok Gondozok { get; set; }
    }
}
