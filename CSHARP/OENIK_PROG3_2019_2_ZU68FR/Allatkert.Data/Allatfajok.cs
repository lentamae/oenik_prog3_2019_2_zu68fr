// <copyright file="Allatfajok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Class that VS made.
    /// </summary>
    [Table("Allatfajok")]
    public partial class Allatfajok
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Allatfajok"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "<Pending>")]
        public Allatfajok()
        {
            this.Allatok = new HashSet<Allatok>();
        }

        /// <summary>
        /// Gets or Sets FajID.
        /// </summary>
        [Key]
        [Column(TypeName = "numeric")]
        public decimal FajID { get; set; }

        /// <summary>
        /// Gets or sets FajNev.
        /// </summary>
        [Required]
        [StringLength(30)]
        public string FajNev { get; set; }

        /// <summary>
        /// Gets or Sets Taplalkozas.
        /// </summary>
        [StringLength(20)]
        public string Taplalkozas { get; set; }

        /// <summary>
        /// Gets or sets Allatok.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "<Pending>")]
        public virtual ICollection<Allatok> Allatok { get; set; }
    }
}
