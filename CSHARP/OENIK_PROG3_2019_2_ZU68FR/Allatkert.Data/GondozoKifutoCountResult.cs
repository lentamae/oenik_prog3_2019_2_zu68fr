﻿// <copyright file="GondozoKifutoCountResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// class for nonCRUD.
    /// </summary>
    public class GondozoKifutoCountResult
    {
        /// <summary>
        /// gets or sets.
        /// </summary>
        public string GondozoId { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public int KifutoDb { get; set; }

        /// <summary>
        /// overrides Equals.
        /// </summary>
        /// <param name="obj"> other result.</param>
        /// <returns>if they equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is GondozoKifutoCountResult)
            {
                GondozoKifutoCountResult other = obj as GondozoKifutoCountResult;
                return this.GondozoId == other.GondozoId && this.KifutoDb == other.KifutoDb;
            }

            return false;
        }

        /// <summary>
        /// overrides gethashcode.
        /// </summary>
        /// <returns> hashcode. </returns>
        public override int GetHashCode()
        {
            return this.GondozoId.GetHashCode() + this.KifutoDb.GetHashCode();
        }

        /// <summary>
        /// tostring override.
        /// </summary>
        /// <returns>string for writing out.</returns>
        public override string ToString()
        {
            return $"A {this.GondozoId} azonosítójú gondozó összesen {this.KifutoDb} kifutóval foglalkozik";
        }
    }
}