﻿// <copyright file="AllatEsFajResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    /// <summary>
    /// Helper class for nonCRUD query.
    /// </summary>
    public class AllatEsFajResult
    {
        /// <summary>
        /// Gets or sets animal's name for the query.
        /// </summary>
        public string AllatNeve { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.AllatNeve}";
        }
    }
}
