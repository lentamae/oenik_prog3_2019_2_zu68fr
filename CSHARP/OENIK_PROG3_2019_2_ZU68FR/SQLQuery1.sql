﻿CREATE TABLE Allatfajok(
fajID NUMERIC PRIMARY KEY,
fajNev VARCHAR(30) NOT NULL UNIQUE,
taplalkozas VARCHAR(20),
CONSTRAINT tapl CHECK (taplalkozas ='Növényevő' OR taplalkozas ='Ragadozó' OR taplalkozas ='Mindenevő')
);
 
CREATE TABLE Gondozok(
gondozoID NUMERIC  PRIMARY KEY,
gondozoNev VARCHAR(30) NOT NULL,
telefonszam VARCHAR(20) NOT NULL,
munkaviszKezd DATE NOT NULL,
munkaviszVeg DATE
);

CREATE TABLE Kifutok(
kifutoID NUMERIC  PRIMARY KEY,
kifutoNev VARCHAR(30) NOT NULL,
kifutoMeret VARCHAR(30) NOT NULL,
gondozoID NUMERIC NOT NULL REFERENCES Gondozok(gondozoID)
);

CREATE TABLE Allatok(
allatID NUMERIC  PRIMARY KEY,
allatNev VARCHAR(30),
fajID NUMERIC REFERENCES Allatfajok(fajID) NOT NULL,
kifutoID NUMERIC REFERENCES Kifutok(kifutoID) NOT NULL,
nem VARCHAR(10) NOT NULL,
szuletesiIdo DATE NOT NULL,
halolozasiIdo DATE,
CONSTRAINT nem CHECK (nem ='Hím' OR nem ='Nőstény')
);

INSERT INTO Allatfajok (fajID,FAJNEV,taplalkozas) VALUES(1,'Kutya','Ragadozó');
INSERT INTO Allatfajok (fajID,FAJNEV,taplalkozas) VALUES(2,'Ciculi','Mindenevő');
INSERT INTO Allatfajok (fajID,FAJNEV,taplalkozas) VALUES(3,'Jegesmedve','Ragadozó');
INSERT INTO Allatfajok (fajID,FAJNEV,taplalkozas) VALUES(4,'Koala','Növényevő');
INSERT INTO Allatfajok (fajID,FAJNEV,taplalkozas) VALUES(5,'Zsiráf','Növényevő');

INSERT INTO Gondozok (gondozoID,gondozoNev,telefonszam,munkaviszKezd) VALUES (1,'Kovács János','063012345678',CONVERT(DATETIME, '2012.06.05'));
INSERT INTO Gondozok (gondozoID,gondozoNev,telefonszam,munkaviszKezd) VALUES (2,'Jóska Pista','063018765431',CONVERT(DATETIME,'2013.10.03'));
INSERT INTO Gondozok (gondozoID,gondozoNev,telefonszam,munkaviszKezd,munkaviszVeg) VALUES (3,'Nagy Imre','06305554433',CONVERT(DATETIME,'2009.01.01'),CONVERT(DATETIME,'2014.04.23'));
INSERT INTO Gondozok (gondozoID,gondozoNev,telefonszam,munkaviszKezd) VALUES (4,'Mr. Géza','062018765431',CONVERT(DATETIME,'2019.10.03'));
INSERT INTO Gondozok (gondozoID,gondozoNev,telefonszam,munkaviszKezd) VALUES (5,'Marinéni','06704439124',CONVERT(DATETIME,'1994.07.19'));

INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (1,'Mackó kuckó','25x30',1);
INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (2,'Harap lak','30x30',1);
INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (3,'Zsiráfkert','40x55',2);
INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (4,'Látványkifutó','100x100',3);
INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (5,'Mackó kuckó2','30x40',4);
INSERT INTO Kifutok (kifutoID,kifutoNev,kifutoMeret,gondozoID) VALUES (6,'Simogató','30x30',5);

INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (1,'Laci',3,1,'Hím',CONVERT(DATETIME,'2010.07.19'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (2,'Luci',3,1,'Nőstény',CONVERT(DATETIME,'2010.07.19'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (3,'Fagyi',3,1,'Hím',CONVERT(DATETIME,'2014.10.10'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (4,'Lusti',4,4,'Hím',CONVERT(DATETIME,'2017.01.25'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo,halolozasiIdo) VALUES (5,'Nyiff',5,3,'Nőstény',CONVERT(DATETIME,'2014.02.21'),CONVERT(DATETIME,'2014.02.22'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo,halolozasiIdo) VALUES (6,'Nyaff',5,3,'Nőstény',CONVERT(DATETIME,'2014.09.11'),CONVERT(DATETIME,'2014.09.12'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (7,'Folti',1,6,'Hím',CONVERT(DATETIME,'2018.11.19'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (8,'Pötyi',1,6,'Hím',CONVERT(DATETIME,'2018.11.19'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo) VALUES (9,'Tejfel',2,6,'Hím',CONVERT(DATETIME,'2019.02.07'));
INSERT INTO Allatok (allatID,allatNev,fajID,kifutoID,nem,szuletesiIdo,halolozasiIdo) VALUES (10,'Lukrécia',2,6,'Nőstény',CONVERT(DATETIME,'1972.12.23'),CONVERT(DATETIME,'1988.01.02'));