﻿// <copyright file="KifutokRepoisitory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;

    /// <summary>
    /// custom repo.
    /// </summary>
    public class KifutokRepoisitory : Repository<Kifutok>, IKifutokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KifutokRepoisitory"/> class.
        /// </summary>
        /// <param name="ctx">base.</param>
        public KifutokRepoisitory(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// dels one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public override bool Delete(int id)
        {
            Kifutok del = this.GetOne(id);

            if (del != null)
            {
                this.ctx.Set<Kifutok>().Remove(del);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// gets one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        public override Kifutok GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.KifutoID == id);
        }

        /// <summary>
        /// inserts one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="kifutoNev">name.</param>
        /// <param name="meret">size.</param>
        /// <param name="gondozoId">caretakerID.</param>
        /// <returns>success.</returns>
        public bool InsertKifutok(int id, string kifutoNev, string meret, int gondozoId)
        {
            Kifutok insert = new Kifutok()
            {
                KifutoID = id,
                KifutoNev = kifutoNev,
                GondozoID = gondozoId,
                KifutoMeret = meret,
            };
            this.ctx.Set<Kifutok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// updates one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateKifutok(int id, string ujnev)
        {
            var update = this.GetOne(id);
            if (update != null)
            {
                update.KifutoNev = ujnev;
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// custom nonCRUD method yey.
        /// </summary>
        /// <returns>custom class result.</returns>
        public IQueryable<GondozoKifutoCountResult> GondozoKifutoCount()
        {
            var eredmeny = from kifutok in this.GetAll()
                           group kifutok by kifutok.GondozoID into csoport
                           select new GondozoKifutoCountResult()
                           {
                               GondozoId = csoport.Key.ToString(),
                               KifutoDb = csoport.Select(x => x.GondozoID).Count(),
                           };
            return eredmeny;
        }
    }
}
