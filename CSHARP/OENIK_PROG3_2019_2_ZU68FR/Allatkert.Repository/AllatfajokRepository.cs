﻿// <copyright file="AllatfajokRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using Allatkert.Data;

    /// <summary>
    /// repo class file for allatok.
    /// </summary>
    public class AllatfajokRepository : Repository<Allatfajok>, IAllatfajokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllatfajokRepository"/> class.
        /// </summary>
        /// <param name="ctx">base.</param>
        public AllatfajokRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// dels one from the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>del one.</returns>
        public override bool Delete(int id)
        {
            Allatfajok del = this.GetOne(id);

            if (del != null)
            {
                this.ctx.Set<Allatfajok>().Remove(del);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// returns one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        public override Allatfajok GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.FajID == id);
        }

        /// <summary>
        /// inserts into db one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="fajNev">speciesName.</param>
        /// <param name="taplalkozas">food type.</param>
        /// <returns>success.</returns>
        public bool InsertAllatfajok(int id, string fajNev, string taplalkozas)
        {
            Allatfajok insert = new Allatfajok()
            {
                FajID = id,
                FajNev = fajNev,
                Taplalkozas = taplalkozas,
            };

            this.ctx.Set<Allatfajok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// updates one item in db.
        /// </summary>
        /// <param name="id">db.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateAllatfajok(int id, string ujnev)
        {
            var update = this.GetAll().SingleOrDefault(x => x.FajID == id);
            if (update != null)
            {
                update.FajNev = ujnev;
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
