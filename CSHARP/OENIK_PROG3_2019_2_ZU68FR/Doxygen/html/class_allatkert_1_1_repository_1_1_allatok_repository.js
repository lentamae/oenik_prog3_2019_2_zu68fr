var class_allatkert_1_1_repository_1_1_allatok_repository =
[
    [ "AllatokRepository", "class_allatkert_1_1_repository_1_1_allatok_repository.html#adc9d01e577a7f72f3281daca7cf792ad", null ],
    [ "AllatEsTaplalkozas", "class_allatkert_1_1_repository_1_1_allatok_repository.html#adafd0f2051c36178e1f60e3a12823d10", null ],
    [ "AllatfajosDarab", "class_allatkert_1_1_repository_1_1_allatok_repository.html#a69da21063097d8a2419d0c97f8911a80", null ],
    [ "Delete", "class_allatkert_1_1_repository_1_1_allatok_repository.html#a267effc6609f72fcff0e212e8723e3ba", null ],
    [ "GetOne", "class_allatkert_1_1_repository_1_1_allatok_repository.html#a6d0a5dea44603fa3fb22c1e20ee1aca8", null ],
    [ "InsertAllatok", "class_allatkert_1_1_repository_1_1_allatok_repository.html#abfa7f480c604604790082fcc960fa95e", null ],
    [ "InsertAllatok", "class_allatkert_1_1_repository_1_1_allatok_repository.html#ab0b3f4cdb305e800c2d22b3f201cf796", null ],
    [ "UpdateAllatok", "class_allatkert_1_1_repository_1_1_allatok_repository.html#aea6e76df3d6e08cb6037c7ba17c1f38b", null ]
];