var namespace_allatkert_1_1_logic =
[
    [ "AllatfajokLogic", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html", "class_allatkert_1_1_logic_1_1_allatfajok_logic" ],
    [ "AllatokLogic", "class_allatkert_1_1_logic_1_1_allatok_logic.html", "class_allatkert_1_1_logic_1_1_allatok_logic" ],
    [ "GondozokLogic", "class_allatkert_1_1_logic_1_1_gondozok_logic.html", "class_allatkert_1_1_logic_1_1_gondozok_logic" ],
    [ "IAllatfajokLogic", "interface_allatkert_1_1_logic_1_1_i_allatfajok_logic.html", "interface_allatkert_1_1_logic_1_1_i_allatfajok_logic" ],
    [ "IAllatokLogic", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html", "interface_allatkert_1_1_logic_1_1_i_allatok_logic" ],
    [ "IGondozokLogic", "interface_allatkert_1_1_logic_1_1_i_gondozok_logic.html", "interface_allatkert_1_1_logic_1_1_i_gondozok_logic" ],
    [ "IKifutokLogic", "interface_allatkert_1_1_logic_1_1_i_kifutok_logic.html", "interface_allatkert_1_1_logic_1_1_i_kifutok_logic" ],
    [ "KifutokLogic", "class_allatkert_1_1_logic_1_1_kifutok_logic.html", "class_allatkert_1_1_logic_1_1_kifutok_logic" ]
];