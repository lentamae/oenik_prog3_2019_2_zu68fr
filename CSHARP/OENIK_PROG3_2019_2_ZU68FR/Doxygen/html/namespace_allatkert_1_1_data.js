var namespace_allatkert_1_1_data =
[
    [ "AllatEsFajResult", "class_allatkert_1_1_data_1_1_allat_es_faj_result.html", "class_allatkert_1_1_data_1_1_allat_es_faj_result" ],
    [ "Allatfajok", "class_allatkert_1_1_data_1_1_allatfajok.html", "class_allatkert_1_1_data_1_1_allatfajok" ],
    [ "Allatok", "class_allatkert_1_1_data_1_1_allatok.html", "class_allatkert_1_1_data_1_1_allatok" ],
    [ "FajDbResult", "class_allatkert_1_1_data_1_1_faj_db_result.html", "class_allatkert_1_1_data_1_1_faj_db_result" ],
    [ "Gondozok", "class_allatkert_1_1_data_1_1_gondozok.html", "class_allatkert_1_1_data_1_1_gondozok" ],
    [ "GondozoKifutoCountResult", "class_allatkert_1_1_data_1_1_gondozo_kifuto_count_result.html", "class_allatkert_1_1_data_1_1_gondozo_kifuto_count_result" ],
    [ "Kifutok", "class_allatkert_1_1_data_1_1_kifutok.html", "class_allatkert_1_1_data_1_1_kifutok" ],
    [ "Model1", "class_allatkert_1_1_data_1_1_model1.html", "class_allatkert_1_1_data_1_1_model1" ]
];