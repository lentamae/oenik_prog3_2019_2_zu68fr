var class_allatkert_1_1_data_1_1_allatok =
[
    [ "Allatfajok", "class_allatkert_1_1_data_1_1_allatok.html#a79a3bbf13194b0bb2f07beedd9df4df1", null ],
    [ "AllatID", "class_allatkert_1_1_data_1_1_allatok.html#aff8f2f96c64adcc0314a6955bea674e2", null ],
    [ "AllatNev", "class_allatkert_1_1_data_1_1_allatok.html#a7a832c32a5348017c4f7cb8608356b44", null ],
    [ "FajID", "class_allatkert_1_1_data_1_1_allatok.html#adb6d69442e2d525255cff830d6ec2ae3", null ],
    [ "HalolozasiIdo", "class_allatkert_1_1_data_1_1_allatok.html#a12b785710c3b0d51c095d8201d5ed9b2", null ],
    [ "KifutoID", "class_allatkert_1_1_data_1_1_allatok.html#ae32d82758e58c8913a884992cdd141a1", null ],
    [ "Kifutok", "class_allatkert_1_1_data_1_1_allatok.html#aeb2917f308743530c763579a899207b8", null ],
    [ "Nem", "class_allatkert_1_1_data_1_1_allatok.html#aaaf714697de5b7cfc0f60d1565b95709", null ],
    [ "SzuletesiIdo", "class_allatkert_1_1_data_1_1_allatok.html#a25df9f095fb89d06aa405c9335baca67", null ]
];