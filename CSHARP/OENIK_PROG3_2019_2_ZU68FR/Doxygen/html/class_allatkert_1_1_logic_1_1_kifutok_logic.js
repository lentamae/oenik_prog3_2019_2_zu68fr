var class_allatkert_1_1_logic_1_1_kifutok_logic =
[
    [ "KifutokLogic", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#a586c8db3cbeefa09569418c5277572eb", null ],
    [ "KifutokLogic", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#ae2e3b706828eb1daec14adc2596c62d6", null ],
    [ "DeleteKifutok", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#afd90855499a5925b021eaf9b6accac16", null ],
    [ "GetAllKifutok", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#a37cd625b062f7607db0684a17e44dcac", null ],
    [ "GetOneKifutok", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#a747b442e16d1a9db95804d99fb317420", null ],
    [ "GondozoKifutoCount", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#ac355627e0a515cb681edf2b20af521cd", null ],
    [ "InsertKifutok", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#a84c33ca0f0bc7146955093675f03697d", null ],
    [ "UpdateKifutok", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#ab9c6e78451730fdb80db2f107b7a57bb", null ],
    [ "GetRepo", "class_allatkert_1_1_logic_1_1_kifutok_logic.html#a685efb1743562c3c5e56b71252345f86", null ]
];