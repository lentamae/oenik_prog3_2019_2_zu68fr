var namespace_allatkert_1_1_repository =
[
    [ "AllatfajokRepository", "class_allatkert_1_1_repository_1_1_allatfajok_repository.html", "class_allatkert_1_1_repository_1_1_allatfajok_repository" ],
    [ "AllatokRepository", "class_allatkert_1_1_repository_1_1_allatok_repository.html", "class_allatkert_1_1_repository_1_1_allatok_repository" ],
    [ "GondozokRepository", "class_allatkert_1_1_repository_1_1_gondozok_repository.html", "class_allatkert_1_1_repository_1_1_gondozok_repository" ],
    [ "IAllatfajokRepository", "interface_allatkert_1_1_repository_1_1_i_allatfajok_repository.html", "interface_allatkert_1_1_repository_1_1_i_allatfajok_repository" ],
    [ "IAllatokRepository", "interface_allatkert_1_1_repository_1_1_i_allatok_repository.html", "interface_allatkert_1_1_repository_1_1_i_allatok_repository" ],
    [ "IGondozokRepository", "interface_allatkert_1_1_repository_1_1_i_gondozok_repository.html", "interface_allatkert_1_1_repository_1_1_i_gondozok_repository" ],
    [ "IKifutokRepository", "interface_allatkert_1_1_repository_1_1_i_kifutok_repository.html", "interface_allatkert_1_1_repository_1_1_i_kifutok_repository" ],
    [ "IRepository", "interface_allatkert_1_1_repository_1_1_i_repository.html", "interface_allatkert_1_1_repository_1_1_i_repository" ],
    [ "KifutokRepoisitory", "class_allatkert_1_1_repository_1_1_kifutok_repoisitory.html", "class_allatkert_1_1_repository_1_1_kifutok_repoisitory" ],
    [ "Repository", "class_allatkert_1_1_repository_1_1_repository.html", "class_allatkert_1_1_repository_1_1_repository" ]
];