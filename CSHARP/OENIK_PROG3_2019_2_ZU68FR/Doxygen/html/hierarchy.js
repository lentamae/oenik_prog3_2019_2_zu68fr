var hierarchy =
[
    [ "Allatkert.Data.AllatEsFajResult", "class_allatkert_1_1_data_1_1_allat_es_faj_result.html", null ],
    [ "Allatkert.Data.Allatfajok", "class_allatkert_1_1_data_1_1_allatfajok.html", null ],
    [ "Allatkert.Data.Allatok", "class_allatkert_1_1_data_1_1_allatok.html", null ],
    [ "DbContext", null, [
      [ "Allatkert.Data.Model1", "class_allatkert_1_1_data_1_1_model1.html", null ]
    ] ],
    [ "Allatkert.Data.FajDbResult", "class_allatkert_1_1_data_1_1_faj_db_result.html", null ],
    [ "Allatkert.Data.Gondozok", "class_allatkert_1_1_data_1_1_gondozok.html", null ],
    [ "Allatkert.Data.GondozoKifutoCountResult", "class_allatkert_1_1_data_1_1_gondozo_kifuto_count_result.html", null ],
    [ "Allatkert.Logic.IAllatfajokLogic", "interface_allatkert_1_1_logic_1_1_i_allatfajok_logic.html", [
      [ "Allatkert.Logic.AllatfajokLogic", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html", null ]
    ] ],
    [ "Allatkert.Logic.IAllatokLogic", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html", [
      [ "Allatkert.Logic.AllatokLogic", "class_allatkert_1_1_logic_1_1_allatok_logic.html", null ]
    ] ],
    [ "Allatkert.Logic.IGondozokLogic", "interface_allatkert_1_1_logic_1_1_i_gondozok_logic.html", [
      [ "Allatkert.Logic.GondozokLogic", "class_allatkert_1_1_logic_1_1_gondozok_logic.html", null ]
    ] ],
    [ "Allatkert.Logic.IKifutokLogic", "interface_allatkert_1_1_logic_1_1_i_kifutok_logic.html", [
      [ "Allatkert.Logic.KifutokLogic", "class_allatkert_1_1_logic_1_1_kifutok_logic.html", null ]
    ] ],
    [ "Allatkert.Repository.IRepository< T >", "interface_allatkert_1_1_repository_1_1_i_repository.html", [
      [ "Allatkert.Repository.Repository< T >", "class_allatkert_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "Allatkert.Repository.IRepository< Allatfajok >", "interface_allatkert_1_1_repository_1_1_i_repository.html", [
      [ "Allatkert.Repository.IAllatfajokRepository", "interface_allatkert_1_1_repository_1_1_i_allatfajok_repository.html", [
        [ "Allatkert.Repository.AllatfajokRepository", "class_allatkert_1_1_repository_1_1_allatfajok_repository.html", null ]
      ] ]
    ] ],
    [ "Allatkert.Repository.IRepository< Allatok >", "interface_allatkert_1_1_repository_1_1_i_repository.html", [
      [ "Allatkert.Repository.IAllatokRepository", "interface_allatkert_1_1_repository_1_1_i_allatok_repository.html", [
        [ "Allatkert.Repository.AllatokRepository", "class_allatkert_1_1_repository_1_1_allatok_repository.html", null ]
      ] ]
    ] ],
    [ "Allatkert.Repository.IRepository< Gondozok >", "interface_allatkert_1_1_repository_1_1_i_repository.html", [
      [ "Allatkert.Repository.IGondozokRepository", "interface_allatkert_1_1_repository_1_1_i_gondozok_repository.html", [
        [ "Allatkert.Repository.GondozokRepository", "class_allatkert_1_1_repository_1_1_gondozok_repository.html", null ]
      ] ]
    ] ],
    [ "Allatkert.Repository.IRepository< Kifutok >", "interface_allatkert_1_1_repository_1_1_i_repository.html", [
      [ "Allatkert.Repository.IKifutokRepository", "interface_allatkert_1_1_repository_1_1_i_kifutok_repository.html", [
        [ "Allatkert.Repository.KifutokRepoisitory", "class_allatkert_1_1_repository_1_1_kifutok_repoisitory.html", null ]
      ] ]
    ] ],
    [ "Allatkert.Data.Kifutok", "class_allatkert_1_1_data_1_1_kifutok.html", null ],
    [ "Allatkert.Repository.Repository< Allatfajok >", "class_allatkert_1_1_repository_1_1_repository.html", [
      [ "Allatkert.Repository.AllatfajokRepository", "class_allatkert_1_1_repository_1_1_allatfajok_repository.html", null ]
    ] ],
    [ "Allatkert.Repository.Repository< Allatok >", "class_allatkert_1_1_repository_1_1_repository.html", [
      [ "Allatkert.Repository.AllatokRepository", "class_allatkert_1_1_repository_1_1_allatok_repository.html", null ]
    ] ],
    [ "Allatkert.Repository.Repository< Gondozok >", "class_allatkert_1_1_repository_1_1_repository.html", [
      [ "Allatkert.Repository.GondozokRepository", "class_allatkert_1_1_repository_1_1_gondozok_repository.html", null ]
    ] ],
    [ "Allatkert.Repository.Repository< Kifutok >", "class_allatkert_1_1_repository_1_1_repository.html", [
      [ "Allatkert.Repository.KifutokRepoisitory", "class_allatkert_1_1_repository_1_1_kifutok_repoisitory.html", null ]
    ] ]
];