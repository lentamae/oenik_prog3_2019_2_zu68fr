var class_allatkert_1_1_logic_1_1_allatok_logic =
[
    [ "AllatokLogic", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a14b5f3a9cf8122ed41de40535fac317e", null ],
    [ "AllatokLogic", "class_allatkert_1_1_logic_1_1_allatok_logic.html#ada140cdee85f3d0d0e4169b6a8447863", null ],
    [ "AllatEsTaplalkozas", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a7f492717c6d023475742fb1008892cb1", null ],
    [ "AllatfajosDarab", "class_allatkert_1_1_logic_1_1_allatok_logic.html#af52f93594125b1df43a21077ca4a33e0", null ],
    [ "DeleteAllatok", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a4891fd9c913de913a19006a670a37126", null ],
    [ "GetAllAllatok", "class_allatkert_1_1_logic_1_1_allatok_logic.html#ab0c6a4fb213b29ec8dd9bb478f7b834d", null ],
    [ "GetOneAllatok", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a2309db66b302e1f4f665cb9d393aedff", null ],
    [ "InsertAllatok", "class_allatkert_1_1_logic_1_1_allatok_logic.html#abcdf757d946bf42218bba21906f5b1ea", null ],
    [ "UpdateAllatok", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a9d58903bf3af1ffe9a30eca8742bea6b", null ],
    [ "GetRepo", "class_allatkert_1_1_logic_1_1_allatok_logic.html#a2c52a56fad8bf033c9857b8bbc1141b6", null ]
];